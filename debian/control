Source: coccinelle
Section: devel
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Emmanuel Arias <emmanuelarias30@gmail.com>
Build-Depends: camlp4,
               debhelper-compat (=12),
               dh-autoreconf,
               dh-ocaml (>= 1.0.3~),
               dh-python,
               hevea,
               latexmk,
               libmenhir-ocaml-dev (>= 20090204.dfsg),
               libparmap-ocaml-dev (>= 1.0~rc4-5~),
               libpcre-ocaml-dev,
               libpycaml-ocaml-dev (>= 0.82-13~),
               menhir (>= 20090204.dfsg),
               ocaml-best-compilers,
               ocaml-findlib,
               ocaml-nox (>= 3.11.1-3~),
               pkg-config (>= 0.9.0),
               python (>= 2.6.6-3~),
               texlive-fonts-extra,
               texlive-fonts-recommended,
               texlive-latex-base,
               texlive-latex-extra,
               texlive-latex-recommended
Standards-Version: 4.3.0
Homepage: http://coccinelle.lip6.fr
Vcs-Git: https://salsa.debian.org/ocaml-team/coccinelle.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/coccinelle

Package: coccinelle
Architecture: any
Depends: libparmap-ocaml,
         libpycaml-ocaml,
         ocaml-findlib,
         python-glade2,
         python-gobject-2,
         python-gtk2,
         ${misc:Depends},
         ${ocaml:Depends},
         ${python:Depends},
         ${shlibs:Depends}
Pre-Depends: dpkg (>= 1.17.14)
Suggests: coccinelle-doc, vim-addon-manager
Description: semantic patching tool for C
 Coccinelle is a program matching and transformation tool for C.
 The programmer describes the code to match and the transformation to
 perform as a semantic patch, which looks like a standard patch, but can
 transform multiple files at any number of code sites.

Package: coccinelle-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Breaks: coccinelle (<< 1.0.0~rc7.deb-4)
Replaces: coccinelle (<< 1.0.0~rc7.deb-4)
Description: documentation for coccinelle
 Coccinelle is a program matching and transformation tool for C.
 The programmer describes the code to match and the transformation to
 perform as a semantic patch, which looks like a standard patch, but can
 transform multiple files at any number of code sites.
 .
 This package contains examples files and additional documentation in
 PDF format.
